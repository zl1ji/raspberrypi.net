﻿using System;
using System.Runtime.InteropServices;

namespace RaspberryPiDotNet
{
	/// <summary>
	/// Raspberry Pi SPI Interface
	/// </summary>
	public class SPI : IDisposable
	{
		private bool _disposed;

		/// <summary>
		/// Initialize SPI interface
		/// </summary>
		/// <param name="clockDivider">SPI clock divider</param>
		public SPI (ushort clockDivider)
		{
			if (!bcm2835_init ())
				throw new Exception ("Unable to initialize bcm2835.so library");
			bcm2835_spi_begin ();
			bcm2835_spi_setBitOrder (1);      
			bcm2835_spi_setDataMode (3);    
			bcm2835_spi_setClockDivider (clockDivider); 
			bcm2835_spi_chipSelect (0);       
			bcm2835_spi_setChipSelectPolarity (0, 0);
			_disposed = false;
		}

		/// <summary>
		/// SPI interface bi-directional transfer
		/// </summary>
		/// <param name="txBuffer">TX Buffer</param>
		/// <param name="rxBuffer">RX Buffer</param>
		/// <param name="size">Number of bytes to transfer</param>
		public void Transfer(byte[] txBuffer, byte[] rxBuffer, uint size)
		{
			bcm2835_spi_transfernb (txBuffer, rxBuffer, size);
		}

		/// <summary>
		/// Dispose SPI interface
		/// </summary>
		public void Dispose()
		{
			if (!_disposed) {
				bcm2835_spi_end ();
				_disposed = true;
			}
		}

		#region Imported functions

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_init")]
		static extern bool bcm2835_init ();

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_begin")]
		private static extern void bcm2835_spi_begin ();

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_end")]
		private static extern void bcm2835_spi_end ();

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_setBitOrder")]
		private static extern void bcm2835_spi_setBitOrder (byte order);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_setClockDivider")]
		private static extern void bcm2835_spi_setClockDivider (UInt16 divider);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_setDataMode")]
		private static extern void bcm2835_spi_setDataMode (byte mode);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_chipSelect")]
		private static extern void bcm2835_spi_chipSelect (byte cs);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_setChipSelectPolarity")]
		private static extern void bcm2835_spi_setChipSelectPolarity (byte cs, byte active);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_transfer")]
		private static extern byte bcm2835_spi_transfer (byte active);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_transfernb", CallingConvention = CallingConvention.Cdecl)]
		private static extern void bcm2835_spi_transfernb (byte[]tbuf, byte[]rbuf, uint len);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_transfern", CallingConvention = CallingConvention.Cdecl)]
		private static extern void bcm2835_spi_transfern (byte[]buf, uint len);

		[DllImport ("libbcm2835.so", EntryPoint = "bcm2835_spi_writenb", CallingConvention = CallingConvention.Cdecl)]
		private static extern void bcm2835_spi_writenb (byte[]buf, uint len);

		#endregion
	}
}

